/**兼容 requestAnimFrame*/
window.requestAnimFrame = (function () {
    return  window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        };
})();

var counter = 0;
var pw = 320, ph = 250;

function resetRect(ctx, canvas) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}


function progressbar(ctx, color, start_x, start_y) {
    this.x = start_x;
    this.y = start_y;
    this.color = color;

    this.draw = function () {
        ctx.fillStyle = this.color;;
        ctx.fillRect(this.x, this.y, pw, this.height);

    }
}


function imgCanvas(ctx, img, start_x, start_y){
    this.x = start_x;
    this.y = start_y;

    this.draw = function(){
        ctx.drawImage(img, this.x, this.y, 70, 140);
    }
}


function triangle(ctx, start_x, start_y){
    this.color = "red";
    this.x = start_x;
    this.y = start_y;
    this.draw = function (){
        ctx.beginPath();
        ctx.moveTo(this.x, this.y);
        ctx.lineTo(this.x + 15, this.y + 10);
        ctx.lineTo(this.x + 15, this.y - 10);
        ctx.closePath();
        ctx.fillStyle = this.color;
        ctx.fill();
    }
}

function drawRect(bar,ctx,canvas) {
    resetRect(ctx,canvas)
    bar.draw();
}

function drawTriangle(ctx, canvas) {
    resetRect(ctx, canvas);
    tri.draw();
}

function drawImg(ctx,canvas) {
    resetRect(ctx,canvas)
    imgCvs.draw();
}


function autoDraw(bar, ctx) {
    counter++

//  bar.hue += 0.8;
    bar.hue = 126;
    bar.heights += 2;
    if (bar.heights > ph) {
        if (counter > 215) {
            resetRect(ctx);
            bar.hue = 0;
            bar.heights = 0;
            counter = 0;
        }
        else {
            bar.hue = 126;
            bar.heights = ph;
            bar.draw();
        }
    }
    else {
        bar.draw();
    }
}
